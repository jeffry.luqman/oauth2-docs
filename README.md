# MyApp
MyApp menggunakan [OAuth 2.0](https://oauth.net/2) untuk *authentication* dan *authorization* <sup>[note](#note-1)</sup>. MyApp mendukung skenario umum OAuth 2.0 seperti skenario untuk aplikasi web, aplikasi *mobile* dan juga *backend service*.

> <span name="note-1">**note**:</span> terkait kewenangan aplikasi / layanan atas data pengguna, bukan terkait kewenangan pengguna atas fitur aplikasi / layanan

## Memulai
1. Membuat *Project* pada [Developer Console](#), undang pengguna lain jika perlukan.
2. Membuat *Client* sesuai tipe aplikasi (*web*, *mobile*, atau *backend service*).
3. [Mendapatkan otorisasi](#mendapatkan-otorisasi) dari *Authorization Server*.
4. Menggunakan *Access Token* untuk memanggil [API yang diproteksi](#validasi-menggunakan-api).
5. [Memperbarui token](#pembaruan-token) jika diperlukan.

## Alur Protokol

    +--------+                               +---------------+
    |        |--(1)- Authorization Request ->|   Resource    |
    |        |                               |     Owner     |
    |        |<-(2)-- Authorization Grant ---|               |
    |        |                               +---------------+
    |        |
    |        |                               +---------------+
    |        |--(3)-- Authorization Grant -->| Authorization |
    | Client |                               |     Server    |
    |        |<-(4)--------- Token ----------|               |
    |        |                               +---------------+
    |        |
    |        |                               +---------------+
    |        |--(5)----- Access Token ------>|    Resource   |
    |        |                               |     Server    |
    |        |<-(6)--- Protected Resource ---|               |
    +--------+                               +---------------+

1. *Client* melakukan permintaan ke pengguna untuk memberikan otorisasi.
2. Pengguna memberikan otorisasi.
3. *Client* melakukan permintaan token ke *Authorization Server*.
4. *Authorization Server* memberikan *Access Token* dan *Refresh Token* serta *Expiration Time*.
5. *Client* melakukan pemanggilan API menggunakan *Access Token*.
6. API yang terproteksi memberikan respon.

## Mendapatkan otorisasi
Secara umum *end point* untuk mendapatkan otorisasi yaitu `POST /oauth/token`, dengan *body response* sebagai berikut:

* Berhasil

  ```json
  {
    "access_token": "{access_token_in_jwt_format_that_contains_the_user’s_identity_information}",
    "token_type": "Bearer",
    "expired_at": "{expiration_time_in_rfc3339_format}",
    "refresh_token": "{refresh_token}"
  }
  ```

   * **access_token** merupakan kode otentikasi untuk memungkinkan aplikasi mengakses API. *Access token* harus dikirim sebagai *header* `Authorization` ketika mengakses semua api yang di proteksi.
      ```
      Authorization: Bearer {access_token}
      ```
      Access token memiliki masa berlaku selama **1 jam** untuk meminimalisir kemungkinan untuk dimanfaatkan oleh orang-orang yang tidak bertanggung jawab.
   * **token_type** merupakan tipe token yang dikirim pada *header* `Authorization` bersamaan dengan *Access Token* dipisahkan dengan spasi, untuk OAuth 2.0 & JWT nilai nya selalu **Bearer**
   * **expired_at** merupakan waktu waktu kedaluwarsa *Access Token* dalam format [RFC 3339](https://tools.ietf.org/html/rfc6749).
      ```
      "expired_at": "2020-10-29T19:45:13.180539+07:00"
      ```
   * **refresh_token** merupakan token untuk [mendapatkan token baru](#pembaruan-token) ketika *Access Token* sudah tidak berlaku.

* Gagal

  ```json
  {
    "error": {
      "code": 401,
      "message": "Kode otorisasi tidak valid.",
      "detail": {
        "authorization_code": {
          "validation.invalid_authorization_code": "Kode otorisasi tidak valid."
        }
      }
    }
  }
  ```

Namun *body request* nya berbeda-beda tergantung tipe pemberian otorisasi yang digunakan sesuai tipe aplikasi (*web*, *mobile* atau *backend service*) sebagai berikut:

1. **[Client Credentials](https://tools.ietf.org/html/rfc6749#section-4.4)**, cukup menggunakan *client credentials* untuk mendapatkan token, digunakan untuk **backend service**.

   ```
   POST /oauth/token
   ```

   ```json
   {
     "grant_type": "client_credentials",
     "client_id": "{your_client_id}",
     "client_secret": "{your_client_secret}"
   }
   ```

2. **[Authorization Code](https://tools.ietf.org/html/rfc6749#section-1.3.1)**, menggunakan mekanisme pengalihan (*redirect*) untuk mendapatkan token, digunakan untuk **aplikasi web**.

   Untuk tipe pemberian otorisasi *Authorization Code*, sebelum memanggil end point `/oauth/token` perlu melakukan pengalihan (redirect) ke alamat otorisasi terlebih dahulu untuk mendapatkan kode otorisasi.

   1. Lakukan pengalihan (redirect) ke alamat otorisasi.

      ```
      /oauth/authorize?response_type=code&client_id={your_client_id}&redirect_uri={your_redirect_uri}&state={your_any_random_state}
      ```

   2. Selanjutnya *Authorization Server* akan melakukan pengalihan (redirect) kembali ke *redirect_uri* yang dikirim disertai dengan param `code` dan `state`.

      ```
      {your_redirect_uri}?code={auth_code}&state={your_any_random_state}
      ```

   3. Request access token

      ```
      POST /oauth/token
      ```

      ```json
      {
        "grant_type": "authorization_code",
        "client_id": "{your_client_id}",
        "client_secret": "{your_client_secret}",
        "code": "{your_auth_code_from_step_2}"
      }
      ```

3. **[PKCE](https://tools.ietf.org/html/rfc7636)**, merupakan ekstensi dari tipe *Authorization Code* dengan tanpa menggunakan *client_secret* untuk dapat melakukan pertukaran token dengan *public client* secara lebih aman, digunakan untuk **browser-based web app (SPA) & mobile / native app**.

   1. Buat *code verifier* & *code challenge* menggunakan hash SHA256, *code challenge* merupakan base64 encode dari hash SHA256 dari *code verifier*

   2. Lakukan pengalihan (redirect) ke alamat otorisasi.

      ```
      /oauth/authorize?response_type=code&client_id={your_client_id}&redirect_uri={your_redirect_uri}&state={your_any_random_state}&code_challenge={your_any_code_challenge}&code_challenge_method=S256
      ```

   3. Selanjutnya *Authorization Server* akan melakukan pengalihan (redirect) kembali ke *redirect_uri* yang dikirim disertai dengan param `code` dan `state`.

      ```
      {your_redirect_uri}?code={auth_code}&state={your_any_random_state}
      ```

   4. Request access token

      ```
      POST /oauth/token
      ```

      ```json
      {
        "grant_type": "authorization_code",
        "client_id": "{your_client_id}",
        "code_verifier": "{your_code_verifier_from_step_1}",
        "code": "{your_auth_code_from_step_3}"
      }
      ```

## [Pembaruan Token](https://tools.ietf.org/html/rfc6749#section-1.5)
Agar kita tetap dapat mengakses API yang diproteksi tanpa harus meminta pengguna melakukan *login* ulang, diperlukan token lain untuk mendapatkan token baru. Berikut ilustrasi pembaruan token.

    +--------+                                           +---------------+
    |        |--(1)------- Authorization Grant --------->|               |
    |        |                                           |               |
    |        |<-(2)----------- Access Token -------------|               |
    |        |               & Refresh Token             |               |
    |        |                                           |               |
    |        |                            +----------+   |               |
    |        |--(3)---- Access Token ---->|          |   |               |
    |        |                            |          |   |               |
    |        |<-(4)- Protected Resource --| Resource |   | Authorization |
    | Client |                            |  Server  |   |     Server    |
    |        |--(5)---- Access Token ---->|          |   |               |
    |        |                            |          |   |               |
    |        |<-(6)- Invalid Token Error -|          |   |               |
    |        |                            +----------+   |               |
    |        |                                           |               |
    |        |--(7)----------- Refresh Token ----------->|               |
    |        |                                           |               |
    |        |<-(8)----------- Access Token -------------|               |
    +--------+               & Refresh Token             +---------------+

Untuk melakukan pembaruan token, sama seperti melakukan pemberian otorisasi, yaitu menggunakan end point `/oauth/token` namun dengan `body request` sebagai berikut:

```
POST /oauth/token
```

```json
{
  "grant_type": "refresh_token",
  "client_id": "{your_client_id}",
  "client_secret": "{your_client_secret}",
  "refresh_token": "{refresh_token}"
}
```

*Refresh Token* juga memiliki masa berlaku, namun jauh lebih panjang dari *Access Token* yaitu selama **6 bulan**, namun *Refresh Token* juga bisa menjadi tidak valid apabila:

1. *Refresh Token* sudah digunakan untuk mendapatkan token baru
2. Pengguna mencabut akses atas aplikasi yang bersangkutan.
3. *Refresh Token* sudah melewati batas maksimal. *Default* batas maksimal refresh token untuk masing-masing *Client*, dari setiap satu pengguna yaitu 20 Refresh Token (satu pengguna dapat mengakses aplikasi yang sama dari 20 perangkat yang berbeda). Batas maksimal refresh token tersebut dapat diatur lebih lanjut pada pengaturan *Client* dari [Developer Account](#).
4. Pengguna melakukan pergantian kata sandi.

Oleh karena itu *Client* perlu melakukan antisipasi kemungkinan refresh token sudah tidak valid.

## Validasi Access Token

### Validasi menggunakan API
Untuk mengecek validasi token tersebut valid atau tidak serta siapa pemilik token tersebut, cukup dengan memanggil api yang tersedia sesuai scope yang diizinkan, misalnya api profil pengguna.

* Request
  ```
  GET /api/v1/user_profile
  Authorization: Bearer {access_token}
  ```

* Response

   * Berhasil

     Data sensitif yang tampil dalam response api tersebut sesuai dengan scopes yang diizinkan atas aplikasi tersebut.
      ```json
      {
        "id": "8adff8ac-dc38-43be-8f88-b379365c7a36",
        "name": "Jeffry Luqman",
        "phone": "6281299875664",
        "email": "jeffry.luqman@gmail.com",
        "is_phone_verified": true,
        "is_email_verified": true,
        "created_at": "2020-10-29T19:45:13.180539+07:00",
        "updated_at": "2020-10-29T19:45:13.180539+07:00"
      }
      ```

   * Gagal

      ```json
      {
        "error": {
          "code": 401,
          "message": "Token otentikasi tidak valid",
          "detail": {
            "access_token": {
              "validation.invalid_token": "Token otentikasi tidak valid"
            }
          }
        }
      }
      ```

### Validasi menggunakan library JWT
*Access Token* menggunakan standar [JWT](https://tools.ietf.org/html/rfc7519) yang mengandung informasi entitas sehingga sebenarnya penerima tidak perlu memanggil API ke server untuk memvalidasi token, untuk mengecek apakah *Access Token* tersebut valid atau tidak, bisa menggunakan library JWT yang tersedia sesuai bahasa pemrograman yang digunakan, daftar library yang dapat digunakan untuk masing-masing bahasa pemrograman bisa dilihat [disini](https://jwt.io/). Pastikan menggunakan library yang mendukung pengecekan **exp**. Key untuk mengecek token nya valid atau tidak ambil dari env MY_APP_JWT_KEY, nanti tim DevOps yang akan mengisi sesuai dengan key jwt yang digunakan di MyApp dev dan juga prod.

JWT terdiri dari 3 bagian yang dipisahkan dengan titik (.), ketiga bagian tersebut yaitu **Header**, **Payload**, dan **Signature**, hasil *decode* bagian **Payload** setidaknya mencakup informasi sebagai berikut:

```json
{
  "iat": 1611396542,
  "exp": 1893456000,
  "user": {
    "id": "8adff8ac-dc38-43be-8f88-b379365c7a36",
    "name": "Jeffry Luqman"
  },
  "client": {
    "id": "8adff8ac-dc38-43be-8f88-b379365c7a36",
    "name": "MyApp Online Store"
  },
  "scopes": [
    "user_profile:get",
    "user_profile.phone",
    "user_profile.email"
  ]
}
```

*Client* dapat menggunakan informasi tersebut sesuai kebutuhan, apabila ada informasi lain yang diperlukan dari pengguna yang bersangkutan, maka *Client* dapat melakukan pemanggilan API menggunakan *Access Token* tersebut sesuai dengan *scope* yang diizinkan.
